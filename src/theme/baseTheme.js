import { css } from 'styled-components'
import { darken, lighten, rgba, parseToHsl } from 'polished'
import pallete from './pallete'

// BREAKPOINTS AND MEDIA QUIRIES

const addAliases = (arr, aliases) =>
    aliases.forEach((key, i) =>
        Object.defineProperty(arr, key, {
            enumerable: false,
            get() {
                return this[i]
            },
        })
    )

// SPACES

export const space = [0, 4, 8, 16, 24, 32, 64, 128, 256, 512]

const breakpoints = [36, 48, 75].map(n => n + 'em')
const maxContainerWidths = ['100%', 540, 720, 1140]
const gutters = [2, 2, 3] // each number stands for index of spaces
const aliases = ['sm', 'md', 'xl']

const media = breakpoints.reduce((acc, breakpoint, i) => {
    acc[aliases[i]] = (...args) => css`
        @media (min-width: ${breakpoint}) {
            ${css(...args)};
        }
    `
    return acc
}, {})

addAliases(breakpoints, aliases)

// FONTS AND TEXT

export const fonts = {
    sans: 'system-ui, sans-serif',
    mono: 'monospace',
    roboto: 'Roboto, Arial, system-ui, sans-serif',
    gilroy: 'Gilroy, Arial, system-ui, sans-serif',
    bebas: "'Bebas Neue', Arial, system-ui, sans-serif",
}

export const fontSizes = [12, 14, 16, 20, 24, 28, 32, 40, 48, 64, 72]

export const fontWeights = {
    thin: 100,
    light: 300,
    normal: 400,
    medium: 500,
    bold: 700,
    black: 900,
}

const letterSpacings = {
    normal: 'normal',
    tracked: '0.1em',
    tight: '-0.05em',
    mega: '0.25em',
}

const lineHeights = {
    solid: 1,
    title: 1.25,
    copy: 1.5,
}

// RADII

export const radii = [0, 2, 4, 8, 16, 9999, '100%']

// ICONS SIZES

const iconSizes = [16, 18, 24, 36, 48]

// BUTTON SIZES

const buttonSizes = {
    default: '16px',
    small: '14px',
    big: '20px',
}

// BOX SHAHOWS

export const shadows = {
    small: '0 0 4px rgba(0, 0, 0, 0.125)',
    medium: '0 0 16px rgba(0, 0, 0, 0.125)',
    large: '0 0 4px rgba(0, 0, 0, 0.125)',
}

// BORDERS

const borders = [
    0,
    '1px solid',
    '2px solid',
    '3px solid',
    '4px solid',
    '8px solid',
    '16px solid',
    '32px solid',
]

// RATIOS

const ratios = {
    video: '56.25%',
    r4x3: '75%',
    golden: '',
}

// TRANSITIONS

const transitionDelays = {
    small: `60ms`,
    medium: `160ms`,
    large: `260ms`,
    xLarge: `360ms`,
}

const opaticy = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]

// COLORS

const grays = isInverted => {
    let greysObj = {}
    for (let i = 0; i <= 100; i++) {
        if (isInverted) {
            greysObj[i] = darken(i / 100, '#ffffff')
        } else {
            greysObj[i] = lighten(i / 100, '#000000')
        }
    }
    return greysObj
}

const getLightness = amount => parseToHsl(amount).lightness

const setByLightness = (amount, color) => {
    if (getLightness(color) <= 0.3) {
        return lighten(amount, color)
    }
    return darken(amount, color)
}

// Colors
// (there are several 'color weights' just like font weights,
// where 100 is the lightest color withit pallete and 900 is the darkest).
// 'Base' is equal to 500 color weight

export const colors = {
    // Brand colors
    primary: {
        base: pallete.primary[500],
        200: pallete.primary[200],
        800: pallete.primary[800],
    },

    secondary: {
        base: pallete.secondary[500],
        200: pallete.secondary[200],
        800: pallete.secondary[800],
    },

    // Semantic colors
    success: {
        base: pallete.success[500],
        200: pallete.success[200],
        800: pallete.success[800],
    },
    info: {
        base: pallete.info[500],
        200: pallete.info[200],
        800: pallete.info[800],
    },
    warning: {
        base: pallete.warning[500],
        200: pallete.warning[200],
        800: pallete.warning[800],
    },
    error: {
        base: pallete.error[500],
        200: pallete.error[200],
        800: pallete.error[800],
    },

    // Neutral (gray) colors
    neutral: {
        black: '#000000',
        white: '#ffffff',
        gray: pallete.gray,
        getGray: function(amount) {
            return setByLightness(`0.${amount}`, '#000000')
        },
    },
}

// VARIANTS colorHover, bgHover, borderColorHover, colorFocus, bgFocus,
// borderColorFocus, colorPressed, bgPressed, borderColorPressed

export const variants = {
    buttons: {
        prime: css`
            color: white;
            background-color: ${colors.primary.base};
            border: 2px solid;
            border-color: ${colors.primary.base};
            font-weight: bold;
            border-radius: ${radii[5]}px;
            padding: 0.5em 2em;

            &:hover,
            &:focus {
                border-color: ${darken(0.2, colors.primary.base)};
                background-color: ${darken(0.2, colors.primary.base)};
            }

            &:active {
                border-color: ${darken(0.4, colors.primary.base)};
                background-color: ${darken(0.4, colors.primary.base)};
            }

            &:disabled {
                pointer-events: none;
                background-color: #aaaaaa;
                border-color: #aaaaaa;
            }
        `,

        outlinedPrime: css`
            color: ${colors.primary.base};
            border: 2px solid;
            border-color: ${colors.primary.base};
            font-weight: bold;
            border-radius: ${radii[5]}px;
            padding: 0.5em 2em;

            &:hover,
            &:focus {
                color: ${darken(0.2, colors.primary.base)};
                border-color: ${darken(0.2, colors.primary.base)};
                background-color: ${rgba(colors.primary.base, 0.3)};
            }

            &:active {
                border-color: ${darken(0.4, colors.primary.base)};
            }

            &:disabled {
                pointer-events: none;
                border-color: #aaaaaa;
            }
        `,

        textPrime: css`
            color: ${colors.primary.base};
            border: 2px solid transparent;
            font-weight: bold;
            border-radius: ${radii[5]}px;
            padding: 0.5em 2em;

            &:hover,
            &:focus {
                color: ${darken(0.2, colors.primary.base)};
                background-color: ${rgba(colors.primary.base, 0.2)};
            }

            &:active {
                background-color: ${rgba(colors.primary.base, 0.4)};
            }

            &:disabled {
                pointer-events: none;
                border-color: #aaaaaa;
            }
        `,
    },

    badges: {
        default: css`
            color: white;
            font-weight: ${fontWeights.bold};
            text-transform: uppercase;
            border-radius: 9999px;
        `,
        discount: css`
            color: white;
            border-radius: 1em;
        `,
        label: css`
            color: white;
        `,
    },

    inputs: {
        prime: css`
            border: 1px solid;
            border-color: #cccccc;
            border-radius: ${radii[2]}px;
            background-color: #eeeeee;
        `,
        second: css`
            border: 2px solid;
            border-color: #eeeeee;
            border-radius: ${radii[5]}px;
            background-color: #ffffff;
        `,
    },
    textStyles: {
        caps: {
            textTransform: 'uppercase',
        },
        lineThrough: {
            textDecoration: 'line-through',
        },
    },
    colorStyles: {
        success: {},
        info: {},
        warning: {
            color: 'black',
            backgroundColor: 'orange',
        },
        error: {
            color: 'white',
            backgroundColor: 'red',
        },
        disabled: {},
    },
}

const baseComponentsOverrides = {}

const theme = {
    space,
    breakpoints,
    media,
    maxContainerWidths,
    gutters,
    fonts,
    fontSizes,
    fontWeights,
    letterSpacings,
    lineHeights,
    radii,
    iconSizes,
    buttonSizes,
    shadows,
    borders,
    ratios,
    transitionDelays,
    colors,
    ...variants,
    ...baseComponentsOverrides,
}

export default theme
