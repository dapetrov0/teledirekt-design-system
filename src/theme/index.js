import React from 'react'
import { ThemeProvider } from 'styled-components'

import THEME from './baseTheme.js'

import GlobalStyle from './GlobalStyle'

export default ({ children }) => (
    <ThemeProvider theme={THEME}>
        <React.Fragment>
            <GlobalStyle />
            {children}
        </React.Fragment>
    </ThemeProvider>
)
