import styled from 'styled-components'
import { variant } from 'styled-system'
import { Box } from './../index'

export const inputVariant = variant({
    key: 'inputs',
})

const Input = styled(Box)`
    outline: none;
    ${inputVariant};
`

Input.displayName = 'Input'

Input.defaultProps = {
    as: 'input',
    p: '8px 16px',
    fontSize: '16px',
    width: '100%',
}

export default Input
