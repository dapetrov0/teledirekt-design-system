import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";

import Icon from "./Icon";
import check from "./icons/check.inline.svg";

storiesOf("Components/Icon", module)
    .add("default", () => <Icon src={check} onClick={action("click")} />)
    .add("with size", () => <Icon src={check} size={32} />)
    .add("with color", () => <Icon src={check} color="#4e7bf9" />)
    .add("with space", () => <Icon src={check} mr="5px" />)
    .add("no icon", () => <Icon />
    );
