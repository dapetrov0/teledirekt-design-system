import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { style, color, px, space } from 'styled-system'
import noicon from './icons/noicon.inline.svg'

const size = style({
    prop: 'size',
    cssProperty: 'fontSize',
    key: 'iconSizes',
    transformValue: px,
    scale: [16, 18, 24, 36, 48],
})

const BaseIcon = ({ src, className }) =>
    React.createElement(src, {
        className,
    })

BaseIcon.propTypes = {
    className: PropTypes.string,
}

BaseIcon.displayName = 'BaseIcon'

const Icon = styled(BaseIcon)`
    display: inline-flex;
    box-sizing: content-box;
    flex-shrink: 0;
    fill: currentColor;
    ${color}
    ${size}
    ${space}
`

Icon.displayName = 'Icon'

Icon.defaultProps = {
    src: noicon,
    color: 'currentColor',
    size: '1em',
}

Icon.propTypes = {
    src: PropTypes.func.isRequired,
    ...color.propTypes,
    ...space.propTypes,
}

export default Icon
