import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";

import Icon from "./../Icon";

import Checkbox from "./Checkbox";

storiesOf("Atoms/Checkbox", module)
    .add("default", () => <Checkbox />)
    .add("with variant", () => <Checkbox variant='second' defaultChecked />)
    .add("checked", () => <Checkbox defaultChecked />)
    .add("disabled", () => <Checkbox disabled />)
    .add("with label", () => (
        <div>
            <label>
                <Checkbox name="delivery" />
                Телефонный звонок
            </label>
            <label>
                <Checkbox />
                СМС
            </label>
        </div>
    ))
    .add("disabled checked", () => <Checkbox disabled defaultChecked />);
