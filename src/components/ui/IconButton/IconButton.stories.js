import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'

import { IconButton } from './../index'
import trash from './trash.inline.svg'

storiesOf('Components/IconButton', module)
    .add('default', () => (
        <IconButton
            src={trash}
            color="white"
            bg="primary.base"
            borderRadius={5}
            p={2}
            ml="auto"
            onClick={action('click')}
        />
    ))
    .add('without icon', () => <IconButton ml="auto" onClick={action('click')} />)
