import React from 'react'
import { storiesOf } from '@storybook/react'

import { Badge, Flex } from './../index'

storiesOf('Components/Badge', module)
    .add('default', () => <Badge>Скидка</Badge>)
    .add('with variant', () => (
        <Flex>
            <Badge variant="discount" bg="red">
                Скидка
            </Badge>
            <Badge variant="label" bg="green" ml={2}>
                Скидка
            </Badge>
        </Flex>
    ))
