import React from 'react'
import styled from 'styled-components'
import { fontSize, variant } from 'styled-system'
import Box from './../Box'

const badgeStyle = variant({
    key: 'badges',
})

const Badge = styled(Box)({ display: 'inline-flex' }, fontSize, badgeStyle)

Badge.displayName = 'Badge'

Badge.defaultProps = {
    fontSize: 1,
    bg: 'lightBlue',
    py: 1,
    px: 2,
    variant: 'default',
}

Badge.propTypes = {
    ...fontSize.propTypes,
}

export default Badge
