import React from 'react'
import { storiesOf } from '@storybook/react'

import { Row, Col } from './../index'

storiesOf('Components/Row', module).add('default', () => (
    <Row>
        <Col bg="lightblue" m={2}>
            Col 1
        </Col>
        <Col bg="lightblue" m={2}>
            Col 2
        </Col>
        <Col bg="lightblue" m={2}>
            Col 3
        </Col>
    </Row>
))
