import styled from 'styled-components'
import {
    fontFamily,
    fontWeight,
    textAlign,
    lineHeight,
    letterSpacing,
    textStyle
} from 'styled-system'

import Box from './../Box'

const Text = styled(Box)(
    fontFamily,
    fontWeight,
    textAlign,
    lineHeight,
    letterSpacing,
    textStyle,
    props => props.theme.Text
)

Text.propTypes = {
    ...fontFamily.propTypes,
    ...fontWeight.propTypes,
    ...textAlign.propTypes,
    ...lineHeight.propTypes,
    ...letterSpacing.propTypes,
    ...textStyle.propTypes
}

Text.displayName = 'Text'

export default Text
