import React from 'react'
import close from './close.inline.svg'

import IconButton from './../IconButton'

const CloseButton = props => <IconButton src={close} {...props} />

CloseButton.displayName = 'CloseButton'

export default CloseButton
