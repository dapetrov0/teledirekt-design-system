import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'

import { Flex, CloseButton } from './../index'

storiesOf('Components/CloseButton', module).add('default', () => (
    <Flex p={2} color="white" bg="primary.base">
        <CloseButton ml="auto" onClick={action('click')} />
    </Flex>
))
