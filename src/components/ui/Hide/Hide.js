import React from 'react'
import styled from 'styled-components'
import { display } from 'styled-system'
import { ThemeConsumer } from 'styled-components'
import hoistStatics from 'hoist-non-react-statics'
import Box from './../Box'

const mapProps = map => Component => hoistStatics(props => <Component {...map(props)} />, Component)

export const Base = styled(Box)(display)

export const Hide = mapProps(({ xs, sm, md, lg, xl, xxl, display, ...props }) => ({
    display: display || [xs, sm, md, lg, xl, xxl].map(n => (n ? 'none' : 'block')),
    ...props,
}))(Base)

Hide.displayName = 'Hide'

export default Hide
