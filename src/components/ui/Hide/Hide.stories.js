import React from 'react'
import { storiesOf } from '@storybook/react'

import { Hide, Box } from './../index'

storiesOf('Components/Hide', module).add('default', () => (
    <Hide xs sm>
        <Box bg="lightBlue">Привет</Box>
    </Hide>
))
