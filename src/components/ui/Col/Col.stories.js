import React from 'react'
import { storiesOf } from '@storybook/react'

import { Flex, Col } from './../index'

storiesOf('Components/Col', module).add('default', () => (
    <Flex>
        <Col bg="lightblue" m={2}>
            Col 1
        </Col>
        <Col bg="lightblue" m={2}>
            Col 2
        </Col>
        <Col bg="lightblue" m={2}>
            Col 3
        </Col>
    </Flex>
))
