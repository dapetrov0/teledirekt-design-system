import React from 'react'
import styled from 'styled-components'
import { borderColor, borderBottom } from 'styled-system'
import Box from './../Box'

const Devider = styled(Box)({ border: 'none' }, borderBottom, borderColor)

Devider.displayName = 'Devider'

Devider.defaultProps = {
    as: 'hr',
    borderBottom: '1px solid',
    borderColor: '#cccccc',
    mx: '0',
}

export default Devider
