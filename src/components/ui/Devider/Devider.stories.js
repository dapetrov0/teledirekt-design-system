import React from 'react'
import { storiesOf } from '@storybook/react'

import { Devider } from './../index'

storiesOf('Components/Devider', module)
    .add('default', () => <Devider />)
    .add('half width', () => <Devider width={1 / 2} />)
