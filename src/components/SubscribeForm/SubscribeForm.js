import PropTypes from 'prop-types'
import React from 'react'
import PersonalDataCheck from './../PersonalDataCheck'
import { Absolute, Box, Flex, Icon, Link } from './../ui'
import Font from './FontAruarelleDigits'
import arrow from './icons/arrow.inline.svg'
import bgBig from './images/bg-big.jpg'
import bgSm from './images/bg.jpg'
import {
    BottomLogo,
    ButtonSubmit,
    CloseBtn,
    Footer,
    Header,
    Inner,
    Input,
    LikeImage,
    Main,
    Root,
    SvgDiscount,
} from './styled'

class SubscribeForm extends React.Component {
    static defaultProps = {
        discountValue: 25,
    }

    state = {
        showCheck: false,
    }

    onFocus = () => this.setState(state => ({ showCheck: true }))

    onClick = () => this.setState(state => ({ showCheck: true }))

    render() {
        const { discountValue, active } = this.props
        const { showCheck } = this.state

        return (
            <Root>
                <Font />
                <Absolute right={5} top={5}>
                    <CloseBtn />
                </Absolute>
                <Header>
                    <LikeImage />
                    <Header.Title>Спасибо за ваш заказ!</Header.Title>
                    <Header.SubTitle>
                        Мы его получили и в самое ближайшее время свяжемся с вами!
                    </Header.SubTitle>
                </Header>
                <Main bgImage={[bgSm, bgBig]}>
                    <Inner>
                        <Main.Wrapper>
                            <Main.Title>Подпишитесь на нашу рассылку и получите скидку!</Main.Title>
                            <Box mb={2}>
                                <SvgDiscount discountValue={discountValue} />
                            </Box>
                            <Input
                                onFocus={this.onFocus}
                                required
                                placeholder="Введите свой e-mail"
                            />
                            {showCheck && <PersonalDataCheck />}
                        </Main.Wrapper>
                        <Flex>
                            <ButtonSubmit onClick={this.onClick}>Отправить</ButtonSubmit>
                        </Flex>
                    </Inner>
                    <Footer>
                        <Inner>
                            <BottomLogo width={207} mb={3} />
                            <Flex
                                as={Link}
                                textDecoration="none"
                                href="https://teledirekt.ru/"
                                color="white"
                            >
                                Перейти на сайт
                                <Icon ml={2} size={21} src={arrow} />
                            </Flex>
                        </Inner>
                    </Footer>
                </Main>
            </Root>
        )
    }
}

SubscribeForm.propTypes = {
    discountValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
}

export default SubscribeForm
