import { createGlobalStyle } from 'styled-components'
import Aquarelle1 from './digits-Aquarelle.woff2'
import Aquarelle2 from './digits-Aquarelle.woff'

const FontAquarelleDigits = createGlobalStyle`
    @font-face {
    font-family: 'Aquarelle';
    src: local('Aquarelle'),
        url(${Aquarelle2}) format('woff2'),
        url(${Aquarelle1}) format('woff');
    font-style: normal;
    font-weight: 500;
}
`
export default FontAquarelleDigits
