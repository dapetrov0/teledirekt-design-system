import React from 'react'
import { css } from 'styled-components'
import {
    Box,
    Flex,
    Container,
    Row,
    Col,
    Icon,
    Text,
    Heading,
    Image,
    Link,
    Button,
    IconButton,
    Card,
    Position,
    Relative,
    Absolute,
    Fixed,
    BgImage,
    Devider,
    CloseButton,
    Badge,
    Hide,
    Input,
} from './../../ui'

import Logo from './../../Logo'
export const Root = props => <Relative width={[300, 400]} bg="white" {...props} />

export const CloseBtn = props => (
    <Absolute top="1px" right="1px">
        <CloseButton color="white" {...props} />
    </Absolute>
)

export const Main = props => (
    <Card
        bg={props.error ? '#e93b58' : 'primary.base'}
        px={3}
        py={5}
        boxShadow="inset 0 -11px 10px -6px rgba(0,0,0,0.2)"
        borderRadius={'0% 0% 0% 30% / 0% 0% 0% 15% '}
        {...props}
    />
)

export const Title = props => (
    <Heading color="white" fontSize={4} mb={1} textAlign="center" {...props} />
)

export const SubTitle = props => (
    <Text
        color="white"
        fontSize={2}
        fontWeight="light"
        mb={5}
        px={[5, 6]}
        textAlign="center"
        {...props}
    />
)

export const Footer = props => (
    <Flex alignItems="center" flexDirection="column" py={3} px={3} {...props} />
)

export const BottomLogo = props => (
    <Logo
        width={149}
        mb={3}
        css={{
            filter:
                'drop-shadow(1px 0 0 rgba(255,255,255, 0.4)) drop-shadow(0 -1px 0 rgba(255,255,255, 0.4)) drop-shadow(0 1px 0 rgba(255,255,255, 0.4)) drop-shadow(-1px 0 0 rgba(255,255,255, 0.4))',
        }}
        {...props}
    />
)

export const SiteLink = props => (
    <Flex
        as={Link}
        textDecoration="none"
        alignItems="center"
        href="https://teledirekt.ru/"
        color="black"
        {...props}
        css={css``}
    />
)
