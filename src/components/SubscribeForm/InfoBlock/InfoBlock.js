import React from 'react'
import {
    Box,
    Flex,
    Container,
    Row,
    Col,
    Icon,
    Text,
    Heading,
    Image,
    Link,
    Button,
    IconButton,
    Card,
    Position,
    Relative,
    Absolute,
    Fixed,
    BgImage,
    Devider,
    CloseButton,
    Badge,
    Hide,
    Input,
} from './../../ui'
import {
    Root,
    Main,
    Title,
    Inner,
    SubTitle,
    Footer,
    BottomLogo,
    SiteLink,
    CloseBtn,
} from './styled'
import arrow from './icons/arrow.inline.svg'

// import

const ErrorBlock = ({ error, close }) => (
    <Root>
        <CloseBtn onClose={close} />
        <Main error={error}>
            {error ? (
                <Title>
                    Oops! <br />
                    Что-то пошло не так :(
                </Title>
            ) : (
                <Title>Спасибо за подписку!</Title>
            )}
            <SubTitle>
                {error
                    ? 'Пожалуйста, повторите попытку позже.'
                    : 'На ваш адрес отправлено письмо с запросом на подтверждение подписки.'}
            </SubTitle>
        </Main>
        <Footer>
            <BottomLogo />
            <SiteLink>
                Перейти на сайт
                <Icon ml={2} size={18} src={arrow} />
            </SiteLink>
        </Footer>
    </Root>
)

export default ErrorBlock
