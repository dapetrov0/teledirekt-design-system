import React from 'react'
import { css } from 'styled-components'
import { media, bp } from './../../theme/utils'
import Logo from './../Logo'
import {
    Box,
    Button,
    Card,
    CloseButton,
    Flex,
    Heading,
    Image,
    Input as InputBase,
    Relative,
    Text,
} from './../ui'
import like from './images/like.jpg'
import spark from './images/spark.png'

export const Root = props => <Relative width={[300, 600]} bg="white" {...props} />

export const Header = props => <Box p={3} {...props} />

Header.Title = props => <Heading as="div" mb={1} fontSize={[4, 7]} textAlign="center" {...props} />

Header.SubTitle = props => (
    <Text
        fontSize={[2, 3]}
        mb={2}
        textAlign="center"
        mx="auto"
        css={{ maxWidth: '360px' }}
        {...props}
    />
)

export const BottomLogo = props => (
    <Logo
        mb={3}
        css={{
            filter:
                'drop-shadow(1px 0 0 rgba(255,255,255, 0.4)) drop-shadow(0 -1px 0 rgba(255,255,255, 0.4)) drop-shadow(0 1px 0 rgba(255,255,255, 0.4)) drop-shadow(-1px 0 0 rgba(255,255,255, 0.4))',
        }}
    />
)

export const CloseBtn = props => <CloseButton color="black" bg="transparent" {...props} />

export const Main = props => (
    <Card
        as="form"
        backgroundImage={props.bgImage.map(item => `url(${item})`)}
        backgroundPosition="center top"
        pt={[18]}
        css={css`
            position: relative;
            overflow: hidden;
            &::before {
                content: '';
                position: absolute;
                width: 350px;
                left: 50%;
                z-index: 1;
                top: -38px;
                transform: translateX(-50%);
                height: 104px;
                background-image: url(${spark});
            }
        `}
        {...props}
    />
)

Main.Wrapper = props => (
    <Card
        width={[280, 338]}
        px={[11, 43]}
        py={[34]}
        mb={[-46]}
        css={{ border: '3px solid', borderImage: 'linear-gradient(#865e1a, #f0e070) 3' }}
        {...props}
    />
)

export const Input = props => (
    <InputBase
        px={[24]}
        py={[3]}
        mb={[20]}
        css={{ borderRadius: '4px', border: 0 }}
        backgroundColor="#ffffff"
        {...props}
    />
)
export const Inner = props => (
    <Flex
        justifyContent="center"
        alignItems="center"
        flexDirection="column"
        width={['100%', 332]}
        css={{ position: 'relative' }}
        mx="auto"
        px={40}
        {...props}
    />
)

export const LikeImage = props => (
    <Image src={like} mx="auto" mb={1} width={[36, 36, 56]} {...props} />
)

Main.Title = props => <Text color="white" fontSize={2} textAlign="center" mb={2} {...props} />

export const ButtonSubmit = props => (
    <Button
        mx="auto"
        fontWeight="light"
        bg="primary.base"
        color="white"
        borderRadius="9999px"
        px="3em"
        py="1em"
        mb={4}
        mt={18}
        css={{
            ':disabled': {
                cursor: 'default',
                backgroundColor: '#929fc1',
            },
        }}
        {...props}
    />
)

export const Footer = props => <Card borderTop="1px solid #c37d0b" py={3} {...props} />

export const SvgDiscount = ({ discountValue }) => (
    <svg viewBox="0 0 200 110" width="100%" height="100%">
        <defs>
            <linearGradient id="gradient" x1="0" x2="0" y1="0" y2="100%">
                <stop stopColor="#f0e171" offset="0%" />
                <stop stopColor="#b9790d" offset="100%" />
            </linearGradient>
        </defs>
        <text
            x="100"
            y="75"
            fill="url(#gradient)"
            fontSize="90"
            textAnchor="middle"
            fontFamily="Aquarelle"
            fontWeight="bold"
        >
            -{discountValue}
            <tspan fontSize="43" dy="-40" dx="10">
                %
            </tspan>
        </text>
        <text
            x="100"
            y="105"
            fontSize="16"
            textAnchor="middle"
            fontFamily="Roboto"
            fill="url(#gradient)"
        >
            на следующий заказ!
        </text>
    </svg>
)
