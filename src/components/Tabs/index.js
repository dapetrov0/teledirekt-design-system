import React from 'react'
import styled from 'styled-components'

// Tabs
class Tabs extends React.Component {
  renderChildren = () => {
    return React.Children.map(this.props.children, child => {
      return React.cloneElement(child, {
        toggled: child.props.tab == child.props.toggledTabIndex,
      })
    })
  }

  render() {
    return this.renderChildren()
  }
}

// Tab
class TabTab extends React.Component {
  onClick = () => {
    const { tab, onClick, disabled } = this.props
    onClick(tab)
  }

  renderChildren = () => {
    return React.Children.map(this.props.children, child => {
      return React.cloneElement(child, {
        onClick: this.props.disabled ? null : this.onClick,
        toggled: this.props.toggled,
      })
    })
  }

  render() {
    return this.renderChildren()
  }
}

Tabs.Tab = TabTab

// Tabs Container
class TabsContainer extends React.Component {
  compnentDidMount() {}

  onToggleTab = tab => {
    this.setState((state, props) => {
      return {
        toggledTabIndex: +tab,
      }
    })
  }

  Prev = () => {
    this.setState((state, props) => {
      const lowBorder = state.toggledTabIndex === 1
      return {
        toggledTabIndex: lowBorder ? 1 : state.toggledTabIndex - 1,
      }
    })
  }

  Next = () => {
    this.setState((state, props) => {
      const topBorder = state.toggledTabIndex === state.tabsCount
      return {
        toggledTabIndex: topBorder ? state.tabsCount : state.toggledTabIndex + 1,
      }
    })
  }

  state = {
    toggledTabIndex: +this.props.defaultTabIndex || 1,
    tabsCount: +this.props.tabsCount,
    onClick: this.onToggleTab,
    Prev: this.Prev,
    Next: this.Next,
  }

  render = () => this.props.children(this.state)
}

Tabs.Container = TabsContainer

// Tabs Panel
class TabsPanel extends React.Component {
  render() {
    if (this.props.tab != this.props.toggledTabIndex) return null
    return this.props.children
  }
}

Tabs.Panel = TabsPanel

const TabsArrow = ({ disabled, children, onClick, ...props }) => (
  <div onClick={disabled ? null : onClick} {...props}>
    {children}
  </div>
)

Tabs.Arrow = TabsArrow

export default Tabs
