import React from 'react'
import { css } from 'styled-components'
import LogoBase from './../Logo'
import {
    Absolute,
    Box,
    Button,
    Card,
    CloseButton,
    Flex,
    Heading,
    Image,
    Input as InputBase,
    Text,
} from './../ui'
import bgDesktop from './images/bg-desktop.png'
import bgMobile from './images/bg-mobile.png'
import truck from './images/truck.svg'

export const Root = props => (
    <Card
        mx="auto"
        width={[300, 500]}
        bg="white"
        backgroundImage={[bgMobile, bgDesktop].map(item => `url(${item})`)}
        backgroundPosition="center top"
        backgroundRepeat="no-repeat"
        backgroundSize="100% auto"
        css={{ position: 'relative' }}
        {...props}
    />
)

export const CloseBtn = props => (
    <Absolute right={2} top={2}>
        <CloseButton color="black" bg="transparent" {...props} />
    </Absolute>
)

export const Logo = props => <LogoBase width={[190, 228]} ml="auto" mb={2} {...props} />
export const Inner = props => <Box px={[20, 80]} {...props} />
export const FooterInner = props => <Box px={[20, 12]} {...props} />

export const Header = props => (
    <Card pt={28} height={[106, 118]} css={{ overflow: 'hidden' }} {...props} />
)

Header.Title = props => (
    <Text fontSize={[1, 19]} color="black" mr={[0, '5px']} textAlign="right" {...props} />
)

export const ButtonSelect = props => (
    <Button
        borderRadius={5}
        color="white"
        border="1px solid"
        borderColor="white"
        px={2}
        width="46%"
        py={['8px', '10px']}
        fontSize={['12px', 2]}
        css={
            props.toggled &&
            css`
                color: #0064ed;
                background-color: white;
                box-shadow: inset 0 3px 10px rgba(0, 0, 0, 0.3);
                border: 0;

                &:hover,
                &:focus {
                    box-shadow: none;
                }
            `
        }
        {...props}
    />
)

export const ButtonSubmit = props => (
    <Button
        borderRadius={5}
        ml="auto"
        color="white"
        fontSize={['14px', '18px']}
        bg="black"
        px="48px"
        fontWeight="bold"
        py={'10px'}
        css={css`
            box-shadow: 0 4px 10px rgba(0, 0, 0, 0.3);
            &:hover,
            &:focus {
                box-shadow: 0 6px 10px rgba(0, 0, 0, 0.3);
            }
            &:active {
                background-color: #333333;
            }
        `}
        {...props}
    />
)

export const Main = props => <Card height={[340, 455]} pt={[22, 40]} {...props} />

export const Form = props => <Box as="form" py={14} {...props} />

export const FlexBetween = props => <Flex justifyContent="space-between" {...props} />

export const Input = props => <InputBase border={0} fontSize={[1, 2]} {...props} />

export const Label = props => <Text color="white" fontSize={[1, 2]} mb={1} {...props} />

export const Field = props => <Box mb={['12px', 3]} {...props} />

export const Prices = props => <Flex py={'12px'} {...props} />

export const ButtonWrapper = props => <Field mt={2} {...props} />

Prices.Row = props => (
    <Flex alignItems="center" justifyContent="space-between" borderTop="1px solid" {...props} />
)
Prices.Text = props => (
    <Text fontSize={[1, '18px']} fontWeight="light" color="white" mb={1} {...props} />
)

Prices.Image = props => <Image src={truck} ml={2} width={[82, 128]} {...props} />

Prices.Value = props => <Text as="span" fontWeight="bold" ml={1} {...props} />

export const Footer = props => <Box py={3} {...props} />

Footer.Heading = props => (
    <Heading as="div" fontSize={[1, 2]} mb={3} textAlign="center" {...props} />
)

export const Benefits = props => (
    <Flex
        flexWrap={['wrap', 'nowrap']}
        alignItems={['flex-start']}
        justifyContent="space-between"
        mx={[0, '-5px']}
        {...props}
    />
)


const BenefitsCol = props => (
    <Flex
        alignItems="flex-start"
        mb={2}
        px={[0, '5px']}
        css={css`
            position: relative;
            @media (min-width: 576px) {
                &::after {
                    content: '';
                    position: absolute;
                    height: 40px;
                    width: 1px;
                    background-color: #dfe0e1;
                    right: -5px;
                    top: 50%;
                    transform: translateY(-50%);
                }
            }
        `}
        {...props}
    />
)

Benefits.Credit = props => <BenefitsCol width={[104]} {...props} />
Benefits.Delivery = props => <BenefitsCol width={[140, 195]} {...props} />
Benefits.Payment = props => <BenefitsCol width={[104, 117]} {...props} />
Benefits.Secure = props => (
    <BenefitsCol
        width={[140, 43]}
        alignItems="center"
        pl={['20px', 0]}
        css={css`
            &::after {
                content: none;
            }
        `}
        {...props}
    />
)

Benefits.Heading = props => <Text fontWeight="bold" fontSize={['10px']} {...props} />
Benefits.Text = props => <Text fontSize={['10px']} {...props} mb={1} />
Benefits.Icon = props => <Image src={props.src} width={[24]} {...props} mt={2} mb={1} mr={2} />
Benefits.IconPay = props => <Image src={props.src} width={[24]} {...props} mt={1} mb={1} mr={2} />
Benefits.IconSsl = props => <Image src={props.src} width={[40]} {...props} mt={1} mb={1} mr={2} />
Benefits.TextSsl = props => (
    <Text
        fontWeight="bold"
        fontSize="11px"
        css={{ textTransform: 'uppercase', lineHeight: '1', maxWidth: '70px' }}
        {...props}
    />
)
