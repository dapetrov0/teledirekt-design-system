import React from 'react'
import PropTypes from 'prop-types'
import { Flex, Absolute, Box, Image, Link, Icon, Text, Hide } from './../ui'

import {
    Root,
    CloseBtn,
    Inner,
    Logo,
    Header,
    Form,
    Input,
    Label,
    Field,
    Prices,
    Footer,
    Main,
    Benefits,
    ButtonSelect,
    FlexBetween,
    ButtonSubmit,
    ButtonWrapper,
    FooterInner,
} from './styled'

import Tabs from './../Tabs'

import delivery from './images/delivery.svg'
import cash from './images/cash.svg'
import visa from './images/visa.svg'
import mastercard from './images/mastercard.svg'
import mir from './images/mir.svg'
import sslSecure from './images/ssl-secure.png'
import { borderWidth } from 'polished'

const DeliveryForm = () => (
    <Root>
        <CloseBtn />

        <Header>
            <Inner>
                <Logo />
                <Header.Title>Узнать стоимость доставки онлайн</Header.Title>
            </Inner>
        </Header>
        <Main>
            <Inner>
                <Tabs.Container defaultTabIndex="1">
                    {tabs => (
                        <React.Fragment>
                            <FlexBetween>
                                <Tabs>
                                    <Tabs.Tab tab="1" {...tabs}>
                                        <ButtonSelect>По адресу</ButtonSelect>
                                    </Tabs.Tab>
                                    <Tabs.Tab tab="2" {...tabs}>
                                        <ButtonSelect>По индексу</ButtonSelect>
                                    </Tabs.Tab>
                                </Tabs>
                            </FlexBetween>
                            <Form as="div">
                                <Tabs.Panel tab="1" {...tabs}>
                                    <Field>
                                        <Label htmlFor="city">Город*</Label>
                                        <Input id="city" placeholder="Введите название города" />
                                    </Field>
                                    <Field>
                                        <Label htmlFor="street">Улица*</Label>
                                        <Input id="street" placeholder="Введите название улицы" />
                                    </Field>
                                    <Flex>
                                        <ButtonSubmit>Узнать</ButtonSubmit>
                                    </Flex>
                                </Tabs.Panel>
                                <Tabs.Panel tab="2" {...tabs}>
                                    <Field>
                                        <Label>Индекс*</Label>
                                        <Input placeholder="_ _ _ _ _ _" />
                                    </Field>
                                </Tabs.Panel>
                            </Form>
                        </React.Fragment>
                    )}
                </Tabs.Container>

                <Prices>
                    <Prices.Row>
                        <Box>
                            <Prices.Text>
                                Почта России:
                                <Prices.Value>0 рублей</Prices.Value>
                            </Prices.Text>
                            <Prices.Text>
                                Курьер:
                                <Prices.Value>0 рублей</Prices.Value>
                            </Prices.Text>
                        </Box>
                        <Prices.Image />
                    </Prices.Row>
                </Prices>
            </Inner>
        </Main>
        <Footer>
            <FooterInner>
                <Footer.Heading>Преимущества покупки в Теледирект:</Footer.Heading>
                <Benefits>
                    <Benefits.Credit>
                        <Benefits.Icon src={cash} />
                        <Box>
                            <Benefits.Heading>Рассрочка!</Benefits.Heading>
                            <Benefits.Text>Подробности у оператора</Benefits.Text>
                        </Box>
                    </Benefits.Credit>
                    <Benefits.Delivery>
                        <Benefits.Icon src={delivery} />
                        <Box>
                            <Benefits.Heading>Способы доставки:</Benefits.Heading>
                            <Benefits.Text>
                                Почта России, Курьер, Постамат и Пункт выдачи заказов.
                            </Benefits.Text>
                        </Box>
                    </Benefits.Delivery>
                    <Benefits.Payment>
                        <Box>
                            <Benefits.Heading>Платежные системы:</Benefits.Heading>
                            <Flex alignItems="center">
                                <Benefits.IconPay width={34} src={visa} />
                                <Benefits.IconPay width={25} src={mastercard} />
                                <Benefits.IconPay width={36} src={mir} />
                            </Flex>
                        </Box>
                    </Benefits.Payment>
                    <Benefits.Secure>
                        <Benefits.IconSsl src={sslSecure} />
                        <Hide display={['block', 'none']}>
                            <Benefits.TextSsl>SSL secure connection</Benefits.TextSsl>
                        </Hide>
                    </Benefits.Secure>
                </Benefits>
            </FooterInner>
        </Footer>
    </Root>
)

export default DeliveryForm
