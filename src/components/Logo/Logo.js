import React from 'react'
import { Image } from './../ui'

import teledirekt from './images/teledirekt.svg'

const Logo = props => <Image src={teledirekt} {...props} />

export default Logo
