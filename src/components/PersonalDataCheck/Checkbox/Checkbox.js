import React from "react";
import styled, { css } from "styled-components";
import PropTypes from "prop-types";

const variants = {
    prime: css`
        color: #4e7bf9;
        background: #ffffff;
           border-radius: 5px;
    border: 1px solid #eeeeee;
    `,
};

const IconCheck = props => (
    <svg
        viewBox="0 0 24 24"
        width="1em"
        height="1em"
        fill="currentColor"
        {...props}
    >
        <path d="M19.22 5.6a1.52 1.52 0 0 0-1 .45c-2.83 2.84-5.85 6.06-8.68 9l-3.91-3.28A1.46 1.46 0 0 0 3.77 14l5 4.07a1.46 1.46 0 0 0 1.9-.07c3.14-3.14 6.53-6.81 9.6-9.89a1.44 1.44 0 0 0 0-2 1.39 1.39 0 0 0-1.05-.51z" />
    </svg>
);

const Mark = styled.span`
    display: inline-flex;
    flex-shrink: 0;
    position: relative;
    user-select: none;
    cursor: pointer;
    box-sizing: content-box;
    width: 1em;
    height: 1em;
    font-size: ${props => props.size}px;

    ${variants.prime}
`;

const Input = styled.input`
    appearance: none;
    opacity: 0;
    position: absolute;
    z-index: -2;

    ~ ${Mark} * {
        visibility: hidden;
        opacity: 0;
    }

    &:checked ~ ${Mark} * {
        visibility: visible;
        opacity: 1;
    }
`;

const Checkbox = ({
    checked,
    disabled,
    variant,
    size,
    name,
    value,
    required,
    autofocus,
    onChange,
    ...props
}) => (
    <React.Fragment>
        <Input
            type="checkbox"
            defaultChecked={checked}
      name={name}
      value={value}
      disabled={disabled}
      required={required}
      autoFocus={autofocus}
      onChange={onChange}
        />
        <Mark
            size={size}
            variant={variant}
            checked={checked}
            disabled={disabled}
            role="checkbox"
            {...props}
        >
            <IconCheck />
        </Mark>
    </React.Fragment>
);

Checkbox.defaultProps = {
    size: 20,
    fontSize: "0.875rem",
    defaultChecked: false
};

Checkbox.propTypes = {
    defaultChecked: PropTypes.bool,
    disabled: PropTypes.bool,
    required: PropTypes.bool,
    autofocus: PropTypes.bool,
    variant: PropTypes.string,
    size: PropTypes.number,
    name: PropTypes.string,
    value: PropTypes.string
};

export default Checkbox;
