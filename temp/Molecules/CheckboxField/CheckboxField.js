import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

import Checkbox from "../../Atoms/Checkbox";

const CheckboxFieldStyled = styled.CheckboxField`
    display: ${props => props.block ? 'block' : 'inline-flex'};
    align-items: stretch;
    font-size: 14px;
    cursor: pointer;
`;

const CheckboxField = ({
    inline,
    block,
    children,
    ...props
}) => (
    <CheckboxFieldStyled {...props}>
        {children}
    </CheckboxFieldStyled>
);

CheckboxField.propTypes = {
    children: PropTypes.node
};

export default CheckboxField;
