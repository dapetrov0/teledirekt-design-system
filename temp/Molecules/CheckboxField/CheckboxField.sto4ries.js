import React from "react";
import { storiesOf } from "@storybook/react";

import { action } from "@storybook/addon-actions";

import Checkbox from "../../Atoms/Checkbox";

import CheckboxField from "./CheckboxField";

storiesOf("Molecules/CheckboxField", module).add("default", () => (
    <CheckboxField>
        <Checkbox onChange={action("change")} />
        Телефонный звонок
    </CheckboxField>
));
// .add("checked", () => <CheckboxField isChecked>Привет</CheckboxField>);
