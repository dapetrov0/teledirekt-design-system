import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const LabelStyled = styled.label`
    display: inline-flex;
    align-items: baseline;

`;

const Label = ({children}) => (
    <LabelStyled>
        {children}
    </LabelStyled>
)

Radio.defaultProps = {
    size: 19,
    variant: "prime",
    fontSize: "0.875rem"
};

Radio.propTypes = {
    checked: PropTypes.bool,
    disabled: PropTypes.bool,
    required: PropTypes.bool,
    autofocus: PropTypes.bool,
    variant: PropTypes.string,
    size: PropTypes.number,
    name: PropTypes.string,
    value: PropTypes.string
};

export default Radio;
