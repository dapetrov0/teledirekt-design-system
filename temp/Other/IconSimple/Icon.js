import React from 'react';
import PropTypes from 'prop-types';

import icons from './sprite.js';

const IconSimple = ({ name, size, color, ...props }) => {
    const path = icons[name];
    if (!path) return false
 
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            xlinkHref="http://www.w3.org/1999/xlink"
            viewBox="0 0 24 24"
            width={size}
            height={size}
            fill={color}
            {...props}
        >
        <path d={path} />
        </svg>
    )
}

IconSimple.displayName = 'Icon';

IconSimple.defaultProps = {
    name: 'close',
    size: 24,
    color: 'currentColor',
    width: '1em',
    height: '1em'
}

IconSimple.propTypes = {
    size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    name: PropTypes.oneOf(Object.keys(icons)).isRequired,
    color: PropTypes.string
}


export default IconSimple;