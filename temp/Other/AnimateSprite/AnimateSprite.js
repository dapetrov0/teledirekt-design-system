import React from 'react';
import PropTypes from 'prop-types';

import styled, { keyframes } from 'styled-components';


const animate = keyframes`

    to {
        transform: translateY(-100%);
    }
`;

const Animation = styled.div`
    display: inline-flex;
    position: relative;
    width: ${props => props.width}px;
    height: ${props => (props.height / props.frames)}px;
    overflow: hidden;

    img {
       position: absolute;
       left: 0;
       top: 0; 
       animation: ${animate} ${props => props.duration}ms steps(${props => props.frames}) infinite;
    }
`;

function AnimateSprite({ children, width, height, frames, duration }) {

    return (
        <Animation width={width} height={height} frames={frames} duration={duration}>
            {children}
        </Animation>
    );
}

AnimateSprite.propTypes = {
    children: PropTypes.element.isRequired,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    frames: PropTypes.number.isRequired,
    duration: PropTypes.number,
}

AnimateSprite.defaultProps = {
    children: <img src={require('./images/animation.png')} alt="" />,
    duration: 5000
}

export default AnimateSprite;