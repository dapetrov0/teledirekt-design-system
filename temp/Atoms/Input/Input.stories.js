import React from "react";
import { storiesOf, addDecorator } from "@storybook/react";

import { action } from "@storybook/addon-actions";

import Input from "./Input";


  const widthDecorator = (storyFn) => (
    <div style={{width: '300px'}}>
      { storyFn() }
    </div>
  );
  

storiesOf("Atoms/Input", module)
    .addDecorator(widthDecorator)
    .add("default", () => <Input type="text" label="Фамилия" required onInputChange={action('typed')} />)
    .add("with focus", () => <Input type="text" label="Фамилия" state={'INPUT_FOCUS'} required onInputChange={action('typed')} />)
    .add("with value", () => <Input type="text" label="Фамилия" value={'Иванов'} required onInputChange={action('typed')} />)
    .add("with error", () => <Input type="text" label="Фамилия" value={'Иванов'} state={'INPUT_ERROR'} required onInputChange={action('typed')} />)
    .add("disabled", () => <Input type="text" label="Фамилия" value={'Иванов'} state={'INPUT_DISABLED'} required onInputChange={action('typed')} />)
