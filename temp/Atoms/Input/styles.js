import styled, { css } from "styled-components";
import { em } from "polished";

export const InputWrapper = styled.div`
    position: relative;
    display: inline-flex;
    font-size: ${props => props.theme.sizes.input};
    width: 100%;
    min-width: 200px;
`;

// Label translated mixin

export const InputStyled = styled.input`
    width: 100%;
    border: none;
    outline: none;

    border: ${props => props.theme.variants.input[props.variant].border};
    border-radius: ${props =>
        props.theme.variants.input[props.variant].borderRadius};
    color: ${props => props.theme.variants.input[props.variant].color};
    padding: ${props => props.theme.variants.input[props.variant].padding};
    font-family: ${props =>
        props.theme.variants.input[props.variant].fontFamily};
    font-weight: ${props =>
        props.theme.variants.input[props.variant].fontWeight};
    border-radius: ${props =>
        props.theme.variants.input[props.variant].borderRadius};

    // focus state

    &:focus {
        ${props => props.theme.variants.input[props.variant].focus}}
    }

    ${props =>
        props.state === "INPUT_FOCUS" &&
        props.theme.variants.input[props.variant].focus}
    
    // with value state
    ${props =>
        props.value.length !== 0 &&
        props.theme.variants.input[props.variant].withValue};

    // error state
    ${props =>
        props.state === "INPUT_ERROR" &&
        props.theme.variants.input[props.variant].error};

           
    // disabled state
    ${props =>
        props.state === "INPUT_DISABLED" &&
        props.theme.variants.input[props.variant].disabled};
`;

export const Label = styled.label`
    display: flex;
    position: absolute;
    align-items: center;
    top: 0;
    bottom: 0;
    left: 1.5rem;
    font-weight: bold;
    pointer-events: none;
    transition: all 0.2s;
    color: ${props => props.theme.variants.input[props.variant].color};
`;

export const TooltipError = styled.div`
    position: absolute;
    text-align: center;
    top: 100%;
    left: 50%;
    transform: translateX(-50%);
    border-radius: 2em;
    width: calc(100% - 40px);
    max-width: 250px;
    font-size: 12px;
    color: #ffffff;
    background-color: ${props => props.theme.colors.neutral.gray[0]};
    padding: 0.2em 2em;

    &::before {
        content: "";
        position: absolute;
        left: 50%;
        margin-left: -8px;
        bottom: 100%;
        width: 0;
        height: 0;
        border-style: solid;
        border-width: 0 4px 3px 4px;
        border-color: transparent transparent
            ${props => props.theme.colors.neutral.gray[0]} transparent;
    }
`;
