import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import { requireAllIcons } from "./utils";

export const icons = requireAllIcons(
    require.context("./icons/", true, /\.svg$/)
);

const BaseIcon = ({ name, className, ...props }) => {
    return React.createElement(
        icons[name] || icons[BaseIcon.defaultProps.name],
        {
            className,
            ...props
        }
    );
};

const Icon = styled(BaseIcon)`
    display: inline-flex;
    box-sizing: content-box;
    font-size: ${props =>
        props.theme.iconSizes[props.size] || props.size};
    color: ${props => props.color};
`;

BaseIcon.defaultProps = {
    name: "noicon"
};

BaseIcon.propTypes = {
    name: PropTypes.oneOf(Object.keys(icons)).isRequired,
    className: PropTypes.string
};

Icon.displayName = "Icon";

Icon.defaultProps = {
    color: "currentColor",
    size: "1em"
};

Icon.propTypes = {
    size: PropTypes.string,
    color: PropTypes.string
};

export default Icon;
