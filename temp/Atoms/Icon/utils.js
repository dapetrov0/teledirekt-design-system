export const requireAllIcons = (r) => {
  let namesArray = r.keys().map(name => name.slice(name.lastIndexOf('/') + 1, name.indexOf('.inline')));
  
  return r.keys().map(r).reduce((acc, value, i) => {
    name = namesArray[i];
    acc[name] = value.default;
    return acc;
  }, {});
};

export const getSize = (size) => {
    if(+size  !== parseFloat(size)) {
			return size
		}
	return size + 'px'
};
