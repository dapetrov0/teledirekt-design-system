import React from 'react';
import { storiesOf } from '@storybook/react';
import { withNotes } from '@storybook/addon-notes';
import { action } from '@storybook/addon-actions';

import { ThemeProvider } from 'styled-components';

import Icon, { icons } from './Icon';

// Theme
const main = {
    sizes: {
        icon: {
            xxxxl: "60px"
        }
    }
}

storiesOf('Atoms/Icon', module)
    .add('default', () => <Icon name="menu" onClick={action('click')} />)
    .add('with size', () => <Icon name="menu" size={'24px'} />)
    .add('with color', () => <Icon name="menu" color="#4e7bf9" size={'24'} />)
    .add('with theme', () => <ThemeProvider theme={main}><Icon name="menu" size={'xxxxl'} /></ThemeProvider>)
    .add('no icon', () => <Icon name='blabla' />)
    .add('all icons', () => <div>{Object.keys(icons).map((icon) => <Icon name={icon} size={24} style={{ marginRight: '5px' }} />)}</div>)

