import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import styled from "styled-components";

import Icon from "./../Icon";

const LabelStyled = styled.label`
    display: inline-flex;
    align-items: ${props => props.alignItems || 'baseline'};
    cursor: pointer;

    > input + * {
        margin: ${props => props.left ? '0 0 0 7px' : '0 7px 0 0'};
    }
`;

const Span = styled.span`
  display: inline-flex;
  align-items: center;
`;

import Radio from "./Radio";


storiesOf("Atoms/Radio", module)
    .add("default", () => <Radio />)
    .add("checked", () => <Radio checked />)
    .add("disabled", () => <Radio disabled />)
    .add("with labels", () => (
        <div>
            <LabelStyled>
                <Radio name="delivery" value="post" />
                Почта России
            </LabelStyled>
            <LabelStyled>
                <Radio name="delivery" value="courier" />
                <Span>Курьер</Span>
            </LabelStyled>
        </div>
    ))
    .add("disabled checked", () => <Radio disabled checked />);
